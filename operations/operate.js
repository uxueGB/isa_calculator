const operate = (operateFunction, x,y) =>{

    // Nos aseguramos que sean números
    if(x==""){
        x = 0;
    }

    if(y==""){
        y = 0;
    }

    x = parseInt(x);
    y = parseInt(y);

    // Ejecutamos la operación
    return operateFunction(x,y);
}

module.exports=operate;