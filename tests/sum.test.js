const {test, expect} = require("@jest/globals");
const sum = require("../operations/sum");

test("se suma 6 y 3. Debería devolver 9", () =>{
    //arrange
    const x = 6;
    const y = 3;
    const expected = 9; 

    //act
    const result = sum(x,y);

    //assert
    expect(result).toBe(expected);
});